import React from 'react';

function Aside(props) {
  const { showMenu } = props;
  let sidebarClass = 'sidebarMenu';
  if (showMenu === true) {
    sidebarClass = 'sidebarMenu open';
  }
  return (
    <aside className="Aside">
      <div className={sidebarClass}>
        <ul className="sidebarMenuInner">
          <li>
            <i className="fa fa-home" aria-hidden="true" />
            Home
          </li>
          <li>
            <i className="fa fa-bullhorn" aria-hidden="true" />
            My Account
          </li>
          <li>
            <i className="fa fa-building" aria-hidden="true" />
            My Company
          </li>
          <li>
            <i className="fa fa-cog" aria-hidden="true" />
            My Settings
          </li>
          <li>
            <i className="fa fa-newspaper-o" aria-hidden="true" />
            News
          </li>
          <li>
            <i className="fa fa-area-chart" aria-hidden="true" />
            Analytics
          </li>
        </ul>
      </div>
    </aside>
  );

  // return <></>;
}

export default Aside;
