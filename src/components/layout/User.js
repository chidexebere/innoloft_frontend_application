import React from 'react';
import Aside from './Aside';
import UserInputTabs from '../container/UserInputTab';

function User(props) {
  const { showMenu } = props;
  return (
    <div className="User">
      <Aside showMenu={showMenu} />
      <UserInputTabs />
    </div>
  );
}

export default User;
