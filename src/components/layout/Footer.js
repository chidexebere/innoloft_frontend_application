import React from 'react';

function Footer() {
  return (
    <footer className="site-footer">
      <div className="Footer"> © Chidiebere Onyegbuchulem 2019</div>
    </footer>
  );
}

export default Footer;
