import React from 'react';
import logo from '../../assests/energieloft_logo.png';
import Notification from '../elements/Notification';
import MenuToggle from '../elements/MenuToggle';

function Header(props) {
  const { toggleMenu } = props;
  return (
    <header id="masthead" className="site-header">
      <div className="Header">
        <MenuToggle toggleMenu={toggleMenu} />
        <a
          className="Header__logo"
          href="https://energieloft.de/public/en/about-energieloft-en/"
        >
          <img src={logo} alt="Energieloft Logo" />
        </a>
      </div>

      <nav id="site-navigation" className="main-navigation">
        <Notification displayLanguage lang="EN" />
        <Notification notificationType="fa fa-envelope-o" />
        <Notification notificationType="fa fa-bell-o" />
      </nav>
    </header>
  );
}

export default Header;
