import React from 'react';

function TextArea(props) {
  const { label, name, placeholder, value, onChange, errorMessage } = props;
  return (
    <div className="Text-Area ">
      <label>
        <div className="label-text">{label}</div>
        <textarea
          name={name}
          placeholder={placeholder}
          value={value}
          rows="3"
          cols="33"
          onChange={onChange}
          required
          className={errorMessage.length > 0 ? 'invalid-field' : ''}
        />
        {errorMessage.length > 0 && (
          <span className="validation-error">{errorMessage}</span>
        )}
      </label>
    </div>
  );
}

export default TextArea;
