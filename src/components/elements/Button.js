import React from 'react';

function Button(props) {
  const { name, onClick, type } = props;
  return (
    <div className="Button-With-Label">
      {/*  eslint-disable-next-line react/button-has-type */}
      <button className="Button Button__outline" type={type} onClick={onClick}>
        {name}
      </button>
    </div>
  );
}

export default Button;
