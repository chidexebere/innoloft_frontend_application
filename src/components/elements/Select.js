import React from 'react';

function Select(props) {
  const {
    label,
    name,
    placeholder,
    countries,
    value,
    onChange,
    errorMessage,
    style
  } = props;
  return (
    <div className="Select">
      <label>
        <div className="label-text">{label}</div>

        <select
          id={name}
          name={name}
          value={value}
          onChange={onChange}
          style={style}
          required
          className={errorMessage.length > 0 ? 'invalid-field' : ''}
        >
          <option value="" disabled>
            {placeholder}
          </option>
          {countries.map(options => {
            return (
              <option key={options} value={options} label={options}>
                {options}
              </option>
            );
          })}
        </select>

        {errorMessage.length > 0 && (
          <span className="validation-error">{errorMessage}</span>
        )}
      </label>
    </div>
  );
}

export default Select;
