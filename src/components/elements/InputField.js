import React from 'react';

function InputField(props) {
  const {
    label,
    name,
    type,
    placeholder,
    minLength,
    maxLength,
    style,
    value,
    onChange,
    errorMessage
  } = props;
  return (
    <div className="Input-Field ">
      <label>
        <div className="label-text">{label}</div>
        <input
          name={name}
          type={type}
          placeholder={placeholder}
          required
          className={errorMessage.length > 0 ? 'invalid-field' : ''}
          value={value}
          minLength={minLength}
          maxLength={maxLength}
          style={style}
          onChange={onChange}
        />

        {errorMessage.length > 0 && (
          <span className="validation-error">{errorMessage}</span>
        )}
      </label>
    </div>
  );
}

export default InputField;
