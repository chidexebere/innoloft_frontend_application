import React from 'react';

function Tab(props) {
  const { children } = props;

  return (
    <div className="Tab-list">
      {/* eslint-disable-next-line no-unused-vars */}
      {React.Children.map(children, (child, i) => {
        let className = 'Tab-list--item';
        if (child.key === props.active) {
          className = `${className} Tab-list--active`;
        }
        const label = child.key;
        const handleClick = () => {
          if (label === 'aTab') {
            props.handleTabA();
          }
          if (label === 'bTab') {
            props.handleTabB();
          }
        };

        return (
          <div className={className} onClick={handleClick} role="presentation">
            {child}
          </div>
        );
      })}
    </div>
  );
}

export default Tab;
