import React from 'react';

function MenuToggle(props) {
  const { toggleMenu } = props;
  return (
    <div className="Menu-toggle">
      <input
        type="checkbox"
        className="openSidebarMenu"
        id="openSidebarMenu"
        onClick={toggleMenu}
      />
      <label htmlFor="openSidebarMenu" className="sidebarIconToggle">
        <div className="spinner diagonal-part-1" />
        <div className="spinner horizontal" />
        <div className="spinner diagonal-part-2" />
      </label>
    </div>
  );
}

export default MenuToggle;
