import React from 'react';

function Notification(props) {
  const { notificationType, displayLanguage, lang } = props;

  if (displayLanguage) {
    return (
      <a href="/" className="Notification">
        <i className="fa fa-globe" aria-hidden="true" />
        <span className="Notification__lang">{lang}</span>
      </a>
    );
  }
  return (
    <a href="/" className="Notification">
      <i className={notificationType} aria-hidden="true" />
      <i className="fa fa-circle Notification__badge" aria-hidden="true" />
    </a>
  );
}

export default Notification;
