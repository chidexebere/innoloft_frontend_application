import React from 'react';
import Button from '../elements/Button';

function Confirm(props) {
  const back = e => {
    e.preventDefault();
    props.prevStep();
  };

  const {
    values: { newUser },
    handleFormUpdate,
    formInvalidMessage
  } = props;
  const {
    email,
    firstName,
    lastName,
    address,
    city,
    state,
    postalCode,
    country
  } = newUser;
  const entries = {
    email,
    firstName,
    lastName,
    address,
    city,
    state,
    postalCode,
    country
  };

  return (
    <section className="Confirm">
      <h3>Confirm </h3>
      <p className="Confirm__title">
        Please confirm the information entered are correct
      </p>
      <form className="form" onSubmit={handleFormUpdate} noValidate>
        <h4 className="Confirm__invalid-field">{formInvalidMessage}</h4>
        {Object.entries(entries).map(entry => {
          return (
            <div key={entry.toString()}>
              <p className="Confirm__list">
                {entry[0]}
                <br />
                <span className="Confirm__list entry">{entry[1]}</span>
              </p>
              <hr className="Confirm__separator" />
            </div>
          );
        })}
        <div className="row">
          <Button name="Back" type="button" onClick={back} />
          <Button name="Update" type="submit" />
        </div>
      </form>
    </section>
  );
}

export default Confirm;
