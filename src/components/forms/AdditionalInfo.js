import React from 'react';
import Button from '../elements/Button';
import InputField from '../elements/InputField';
import TextArea from '../elements/TextArea';
import Select from '../elements/Select';

function AdditionalInfo(props) {
  const saveAndContinue = e => {
    e.preventDefault();
    props.nextStep();
  };

  const back = e => {
    e.preventDefault();
    props.prevStep();
    props.changeActiveToTabA();
  };

  const {
    values: { newUser },
    countries,
    handleChange,
    errorMessage
  } = props;
  return (
    <section className="Additional-info">
      <form className="form">
        <InputField
          label="Change First Name"
          name="firstName"
          type="text"
          placeholder="First Name"
          onChange={handleChange}
          value={newUser.firstName}
          errorMessage={errorMessage.firstName}
        />
        <InputField
          label="Change First Name"
          name="lastName"
          type="text"
          placeholder="Last Name"
          onChange={handleChange}
          value={newUser.lastName}
          errorMessage={errorMessage.lastName}
        />

        <TextArea
          label="Change Address"
          name="address"
          placeholder=" House Number, Street"
          onChange={handleChange}
          value={newUser.address}
          errorMessage={errorMessage.address}
        />
        <InputField
          name="city"
          type="text"
          placeholder="City"
          onChange={handleChange}
          value={newUser.city}
          errorMessage={errorMessage.city}
        />
        <InputField
          name="state"
          type="text"
          placeholder="State/Province"
          onChange={handleChange}
          value={newUser.state}
          errorMessage={errorMessage.state}
        />
        <InputField
          name="postalCode"
          type="number"
          placeholder="poster code"
          onChange={handleChange}
          value={newUser.postalCode}
          errorMessage={errorMessage.postalCode}
          style={{ width: '60%' }}
        />
        <Select
          label="Select Country"
          name="country"
          placeholder="Select Country"
          onChange={handleChange}
          value={newUser.country}
          countries={countries}
          errorMessage={errorMessage.country}
          style={{ width: '60%' }}
        />

        <div className="row">
          <Button name="Back" type="button" onClick={back} />
          <Button
            name="Save & Continue "
            type="submit"
            onClick={saveAndContinue}
          />
        </div>
      </form>
    </section>
  );
}

export default AdditionalInfo;
