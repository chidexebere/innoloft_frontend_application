import React from 'react';
import Button from '../elements/Button';
import InputField from '../elements/InputField';
import PasswordStrengthMeter from '../elements/PasswordStrengthMeter';

function MainInfo(props) {
  const saveAndContinue = e => {
    e.preventDefault();
    props.nextStep();
    props.changeActiveToTabB();
  };

  const {
    values: { newUser },
    handleChange,
    errorMessage
  } = props;
  return (
    <section className="Main-info">
      <form className="form">
        <InputField
          label="Change E-Mail"
          name="email"
          type="email"
          placeholder="email@your-company.com"
          onChange={handleChange}
          value={newUser.email}
          errorMessage={errorMessage.email}
        />
        <InputField
          label="Change Password"
          name="password"
          type="password"
          placeholder="********"
          onChange={handleChange}
          value={newUser.password}
          errorMessage={errorMessage.password}
        />
        <PasswordStrengthMeter password={newUser.password} />

        <InputField
          label="Re-Enter New Password"
          name="confirmPassword"
          type="password"
          placeholder="********"
          onChange={handleChange}
          value={newUser.confirmPassword}
          errorMessage={errorMessage.confirmPassword}
        />
        <PasswordStrengthMeter password={newUser.confirmPassword} />

        <div className="row">
          <Button
            name="Save & Continue"
            type="submit"
            onClick={saveAndContinue}
          />
        </div>
      </form>
    </section>
  );
}

export default MainInfo;
