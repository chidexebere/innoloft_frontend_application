import React from 'react';
import Button from '../elements/Button';

function Success(props) {
  const reset = e => {
    e.preventDefault();
    props.restartStep();
  };

  return (
    <section className="Success">
      <h2 className="Success__title"> Your information has been updated.</h2>
      <p className="Success__info">Kindly check your email for details</p>
      <div className="row">
        <Button name="Return to form" type="button" onClick={reset} />
      </div>
    </section>
  );
}

export default Success;
