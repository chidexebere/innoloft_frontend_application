/* eslint-disable no-nested-ternary */
import React, { Component } from 'react';
import MainInfo from '../forms/MainInfo';
import AdditionalInfo from '../forms/AdditionalInfo';
import Confirm from '../forms/Confirm';
import Success from '../forms/Success';
import Tabs from '../elements/Tab';

// Regex for form
const emailRegex = RegExp(
  /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
);

const alphabetsRegex = RegExp(/^[a-zA-Z ]+$/);

const numbersRegex = RegExp(/^\d+$/);

// form validation
const formValid = ({ errorMessage, newUser }) => {
  let valid = true;

  // validate if there are form error messages
  Object.values(errorMessage).forEach(val => {
    if (val.length > 0) {
      valid = false;
    }
  });

  // validate the form was filled out
  Object.values(newUser).forEach(val => {
    if (val === '') {
      valid = false;
    }
  });

  return valid;
};
class UserInputTabs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      step: 1,
      active: 'aTab',
      newUser: {
        email: '',
        password: '',
        confirmPassword: '',
        firstName: '',
        lastName: '',
        address: '',
        city: '',
        state: '',
        postalCode: '',
        country: ''
      },
      errorMessage: {
        email: '',
        password: '',
        confirmPassword: '',
        firstName: '',
        lastName: '',
        address: '',
        city: '',
        state: '',
        postalCode: '',
        country: ''
      },
      formInvalidMessage: '',
      countries: ['Germany', 'Austria', 'Switzerland']
    };
  }

  // Proceed to next step of form
  nextStep = () => {
    this.setState(prevState => {
      const { step } = prevState;
      return {
        step: step + 1
      };
    });
  };

  // Go back to prev step of form
  prevStep = () => {
    this.setState(prevState => {
      const { step } = prevState;
      return {
        step: step - 1
      };
    });
  };

  // Change active tab
  changeActiveToTabA = () => {
    this.setState({ active: 'aTab' });
  };

  changeActiveToTabB = () => {
    this.setState({ active: 'bTab' });
  };

  // Resets form
  resetStep = () => {
    this.setState({
      step: 1,
      active: 'aTab',
      newUser: {
        email: '',
        password: '',
        confirmPassword: '',
        firstName: '',
        lastName: '',
        address: '',
        city: '',
        state: '',
        postalCode: '',
        country: ''
      },
      errorMessage: {
        email: '',
        password: '',
        confirmPassword: '',
        firstName: '',
        lastName: '',
        address: '',
        city: '',
        state: '',
        postalCode: '',
        country: ''
      },
      formInvalidMessage: '',
      countries: ['Germany', 'Austria', 'Switzerland']
    });
  };

  // Handle input fields change and validation
  handleChange = e => {
    const { name, value } = e.target;
    const { errorMessage, newUser } = this.state;

    switch (name) {
      case 'email':
        errorMessage.email = emailRegex.test(value)
          ? ''
          : 'Invalid email address';
        break;
      case 'password':
        errorMessage.password =
          value.length < 6 ? 'Minimum 6 characaters required' : '';
        newUser.password = value;

        break;
      case 'confirmPassword':
        errorMessage.confirmPassword =
          value !== newUser.password ? 'Password does not match' : '';
        break;
      case 'firstName':
        errorMessage.firstName =
          value.length < 2
            ? 'Minimum 2 characaters required'
            : alphabetsRegex.test(value)
            ? ''
            : 'First name should contain alphabets';
        break;
      case 'lastName':
        errorMessage.lastName =
          value.length < 2
            ? 'Minimum 2 characaters required'
            : alphabetsRegex.test(value)
            ? ''
            : 'Last name should contain alphabets';
        break;
      case 'address':
        errorMessage.address = value.length === 0 ? 'Required' : '';
        break;
      case 'city':
        errorMessage.city =
          value.length === 0
            ? 'Required'
            : alphabetsRegex.test(value)
            ? ''
            : 'City should contain alphabets';
        break;
      case 'state':
        errorMessage.state =
          value.length === 0
            ? 'Required'
            : alphabetsRegex.test(value)
            ? ''
            : 'State should contain alphabets';
        break;
      case 'postalCode':
        errorMessage.postalCode =
          value.length === 0
            ? 'Required'
            : numbersRegex.test(value)
            ? ''
            : 'Postal codes (Zip codes) should contain only digits';
        break;
      case 'country':
        errorMessage.country = value.length === 0 ? 'Required' : '';
        break;

      default:
        break;
    }

    this.setState(prevState => ({
      errorMessage,
      newUser: {
        ...prevState.newUser,
        [name]: value
      }
    }));
  };

  // Handles Tab A
  handleTabA = () => {
    this.setState({ step: 1 });
    this.setState({ active: 'aTab' });
  };

  // Handles Tab B
  handleTabB = () => {
    this.setState({ step: 2 });
    this.setState({ active: 'bTab' });
  };

  // Handles form update to a fake server
  handleFormUpdate = e => {
    e.preventDefault();
    const { newUser } = this.state;
    const userData = newUser;
    if (formValid(this.state)) {
      fetch('/api/form-submit-url', {
        method: 'POST',
        body: JSON.stringify(userData),
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        }
      }).then(response => {
        response
          .json()
          .then(data => {
            console.log(`Successful ${data}`);
          })
          .catch(err => console.error(err));
      });
      this.nextStep();
    } else {
      this.setState({
        formInvalidMessage:
          'FORM INVALID - confirm all fields are valid and not empty'
      });
    }
  };

  // eslint-disable-next-line consistent-return
  render() {
    const {
      step,
      newUser,
      countries,
      active,
      errorMessage,
      formInvalidMessage
    } = this.state;
    const values = { newUser };
    const content = {
      aTab: (
        <MainInfo
          nextStep={this.nextStep}
          handleChange={this.handleChange}
          values={values}
          errorMessage={errorMessage}
          changeActiveToTabB={this.changeActiveToTabB}
        />
      ),
      bTab: (
        <AdditionalInfo
          nextStep={this.nextStep}
          prevStep={this.prevStep}
          handleChange={this.handleChange}
          values={values}
          errorMessage={errorMessage}
          changeActiveToTabA={this.changeActiveToTabA}
          countries={countries}
        />
      )
    };

    // eslint-disable-next-line default-case
    switch (step) {
      case 1:
        return (
          <section className="User-input-tab">
            <Tabs
              active={active}
              handleTabA={this.handleTabA}
              handleTabB={this.handleTabB}
            >
              <div key="aTab">Main Information</div>
              <div key="bTab">Additional Information</div>
            </Tabs>

            <div>{content[active]}</div>
          </section>
        );

      case 2:
        return (
          <section className="User-input-tab">
            <Tabs
              active={active}
              handleTabA={this.handleTabA}
              handleTabB={this.handleTabB}
            >
              <div key="aTab">Main Information</div>
              <div key="bTab">Additional Information</div>
            </Tabs>

            <div>{content[active]}</div>
          </section>
        );
      case 3:
        return (
          <section className="User-input-tab">
            <Confirm
              nextStep={this.nextStep}
              prevStep={this.prevStep}
              handleFormUpdate={this.handleFormUpdate}
              values={values}
              formInvalidMessage={formInvalidMessage}
            />
          </section>
        );
      case 4:
        return (
          <section className="User-input-tab">
            <Success restartStep={this.resetStep} />
          </section>
        );
    }
  }
}

export default UserInputTabs;
