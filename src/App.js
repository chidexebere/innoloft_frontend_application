import React from 'react';
import './App.css';
import Header from './components/layout/Header';
import User from './components/layout/User';
import Footer from './components/layout/Footer';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { showMenu: false };
  }

  toggleMenu = () => {
    this.setState(prevState => {
      return { showMenu: !prevState.showMenu };
    });
  };

  render() {
    const { showMenu } = this.state;
    return (
      <div className="App">
        <Header toggleMenu={this.toggleMenu} />
        <User showMenu={showMenu} />
        <Footer />
      </div>
    );
  }
}

export default App;
